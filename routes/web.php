<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/forgot-password', function () {
    return view('forgot-password');
});

Route::get('/reset-password', function () {
    return view('reset-password');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/credit-analysis', function () {
    return view('credit-analysis');
});

Route::get('/search', function () {
    return view('search');
});

Route::get('/add-customer', function () {
    return view('add-customer');
});

Route::get('/add-customer-account-statement', function () {
    return view('add-customer-account-statement');
});

Route::get('/all-customers', function () {
    return view('all-customers');
});

Route::get('/deleted-customers', function () {
    return view('deleted-customers');
});

Route::get('/calculator', function () {
    return view('calculator');
});

Route::get('/account-statement', function () {
    return view('account-statement');
});

Route::get('charts', function () {
    return view('charts');
});