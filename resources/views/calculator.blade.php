@extends('layouts.app')

@section('title', 'Cdl Calculator Logic')

@section('content')
{{-- 
	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<span class="text">Cdl Calculator Logic</span>
		</h2>
	</div>
	 --}}
	<div class="row">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body p-5">
					
					<div class="card-title">Cdl Calculator Logic</div>

					<div class="row">
						<div class="col-md-9 mx-auto">

							<div class="form-group">
								<div class="row">
									<label class="form-label font-weight-light col-md-5">Change Rate</label>
									<div class="col-md-7">
										<input type="text" class="form-control form-control-sm form-control-dark" id="change_rate" value="3.95">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<label class="form-label font-weight-light col-md-5">DTI(50k - 149k)</label>
									<div class="col-md-7">
										<input type="text" class="form-control form-control-sm form-control-dark" id="change_rate" value="0.33">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<label class="form-label font-weight-light col-md-5">DTI(150k - 249k)</label>
									<div class="col-md-7">
										<input type="text" class="form-control form-control-sm form-control-dark" id="change_rate" value="0.40">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<label class="form-label font-weight-light col-md-5">DTI(250k +)</label>
									<div class="col-md-7">
										<input type="text" class="form-control form-control-sm form-control-dark" id="change_rate" value="0.50">
									</div>
								</div>
							</div>
							
						</div>
					</div>


					<div class="form-group">
						<div class="row">
							<div class="col-5 mx-auto">
								<button type="submit" class="btn btn-primary btn-sm btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>

				</div>
			</div>						
		</div>
		
	</div>

@stop