@extends('layouts.app')

@section('title', 'Add A Customer')

@section('content')

	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<span class="text">Add a Customer</span>
		</h2>
	</div>
	
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body p-5">
					
					<div class="form-group">
						<label for="bvn" class="form-label">BVN</label>
						<input type="text" class="form-control" id="bvn" placeholder="2323233234">
					</div>

					<div class="form-group">
						<label for="phone_no" class="form-label">Phone Number</label>
						<input type="text" class="form-control" id="phone_no" placeholder="0801234567890">
					</div>

					<div class="form-group">
						<label for="bvn" class="form-label">Bank</label>
						<select type="text" class="form-control" id="bvn" placeholder="2323233234">
							<option value="">Choose Customer's Bank</option>
						</select>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-6">
								<button type="submit" class="btn btn-primary btn-xl btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>

				</div>
			</div>						
		</div>
		<div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur optio quae accusamus eius aperiam dolore, non, totam quia aut pariatur recusandae facere quisquam?
			</p>

		</div>
	</div>

@stop