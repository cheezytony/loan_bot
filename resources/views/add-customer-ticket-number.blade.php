@extends('layouts.app')

@section('title', 'Add Customer Ticket Number')

@section('content')

	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<span class="text">Add a Customer</span>
		</h2>
	</div>
	
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body p-5">

					<div class="card-title">Ticket Number</div>
					
					<div class="form-group">
						<label for="bvn" class="form-label">Ticket Number</label>
						<input type="text" class="form-contol" id="bvn" placeholder="Ticket Number">
					</div>

					<div class="form-group">
						<label for="phone_no" class="form-label">OTP</label>
						<input type="text" class="form-control" d="phone_no" placeholder="OTP">
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-6">
								<button type="submit" class="btn btn-primary btn-xl btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>

				</div>
			</div>						
		</div>
		<div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur optio quae accusamus eius aperiam dolore, non, totam quia aut pariatur recusandae facere quisquam?
			</p>

		</div>
	</div>

@stop