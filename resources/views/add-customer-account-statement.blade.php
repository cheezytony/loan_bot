@extends('layouts.app')

@section('title', 'Add Customer Ticket Number')

@section('content')

	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<span class="text">Add a Customer</span>
		</h2>
	</div>
	
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body p-5">

					<div class="card-title">Upload Statement</div>
					
					<div class="form-group">
						<label for="file" class="form-label">Account Statement</label>
						<input type="file" class="form-control" id="file" placeholder="Account Statement">
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-6">
								<button type="submit" class="btn btn-primary btn-xl btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>

				</div>
			</div>						
		</div>
		<div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur optio quae accusamus eius aperiam dolore, non, totam quia aut pariatur recusandae facere quisquam?
			</p>

		</div>
	</div>

@stop