@extends('layouts.app')

@section('title', 'All Customers')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">Data Overview</span>
	</h2>

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">
						<span>Account Statement</span>
						<a href="#" class="btn btn-light btn-round btn-sm btn-wide float-right">Filter</a>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Value Date</th>
								<th>Debit</th>
								<th>Credit</th>
								<th>Balance</th>
								<th>Remarks</th>
								<th>Category</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
							<tr>
								<td>May 31, 2018</td>
								<td>305</td>
								<td>500</td>
								<td>1,334</td>
								<td>MTHLY MTCE FEE</td>
								<td>Charges</td>
								<td><button class="btn btn-light btn-round">Select One</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop