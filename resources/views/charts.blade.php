@extends('layouts.app')

@section('title', 'Pick a chart')

@section('content')

	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<span class="text">@yield('title')</span>
		</h2>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<canvas class="new-canvas" id="donut-chart-1">
						Chart Not Supported 
					</canvas>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<canvas class="new-canvas" id="donut-chart-2">
						Chart Not Supported 
					</canvas>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<canvas class="new-canvas" id="donut-chart-3">
						Chart Not Supported 
					</canvas>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<canvas class="new-canvas" id="line-chart-1">
						Chart Not Supported 
					</canvas>
				</div>
			</div>
		</div>
	</div>

@stop

@push('js')
	<script>
		$(function() {
			var randomScalingFactor = function() {
				return Math.round(Math.random() * 100);
			};
			var colors = {
				blue: '#007bff',
				red: '#dc143c',
				yellow: '#f8ca02',
				pink: '#f332cd',
				wine: '#b22222'
			}
			if ($('#donut-chart-1').length) {
				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
								backgroundColor: [colors.blue, colors.red, colors.wine],
								label: 'Dataset 1'
							}
						],
						labels: [
							'Data Entry 1',
							'Data Entry 2',
							'Data Entry 3',
						]
					}
				}
				var ctx = document.querySelector('#donut-chart-1').getContext('2d');
				var chart = new Chart(ctx, config);
			}

			if ($('#donut-chart-2').length) {
				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
								backgroundColor: [colors.blue, colors.red, colors.yellow],
								label: 'Dataset 1'
							}
						],
						labels: [
							'Data Entry 1',
							'Data Entry 2',
							'Data Entry 3',
						]
					}
				}
				var ctx = document.querySelector('#donut-chart-2').getContext('2d');
				var chart = new Chart(ctx, config);
			}

			if ($('#donut-chart-3').length) {
				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [randomScalingFactor(), randomScalingFactor()],
								backgroundColor: [colors.blue, colors.pink],
								label: 'Dataset 1'
							}
						],
						labels: [
							'Data Entry 1',
							'Data Entry 2',
						]
					}
				}
				var ctx = document.querySelector('#donut-chart-3').getContext('2d');
				var chart = new Chart(ctx, config);
			}

			if ($('#line-chart-1').length) {
				var ctx = document.querySelector('#line-chart-1').getContext('2d');

				grd = ctx.createLinearGradient(150.500, 0.000, 150.500, 311.000);

				// Add colors
				grd.addColorStop(0.433, 'rgba(0, 123, 255, 0.500)');
				grd.addColorStop(1.000, 'rgba(0, 123, 255, 0.100)');

				var config = {
					type: 'line',
					data: {
						datasets: [
							{
								data: [
									randomScalingFactor(),
									randomScalingFactor(),
									randomScalingFactor(),
									randomScalingFactor(),
									randomScalingFactor(),
									randomScalingFactor(),
									randomScalingFactor(),
									randomScalingFactor(),
								],
								borderColor: colors.blue,
								backgroundColor: grd,
								label: 'Dataset 1',
							}
						],
						labels: [
							'Data Entry 1',
							'Data Entry 2',
							'Data Entry 3',
							'Data Entry 4',
							'Data Entry 5',
							'Data Entry 6',
							'Data Entry 7',
							'Data Entry 8',
						]
					}
				}
				var chart = new Chart(ctx, config);
			}
		});
	</script>
@endpush